﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenGameListWebApp.Data.Items;

namespace OpenGameListWebApp.ViewModel
{

    public class ItemViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public string Notes { get; set; }
        public int Type { get; set; }
        public int Flags { get; set; }
        public string UserId { get; set; }
        [JsonIgnore]
        public int ViewCount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public ItemViewModel() { }
        public ItemViewModel(Item item) : this()
        {
            Id = item.Id;
            Title = item.Title;
            Description = item.Description;
            Text = item.Text;
            Notes = item.Notes;
            Type = item.Type;
            Flags = item.Flags;
            UserId = item.UserId;
            ViewCount = item.ViewCount;
            CreatedDate = item.CreatedDate;
            LastModifiedDate = item.LastModifiedDate;

        }

        public Item ToEntity()
        {
            return new Item()
            {
                Id = Id,
                Title = Title,
                Description = Description,
                Text = Text,
                Notes = Notes,
                Type = Type,
                Flags = Flags,
                UserId = UserId,
                ViewCount = ViewCount,
                CreatedDate = CreatedDate,
                LastModifiedDate = LastModifiedDate
                
            };
        }
    }
}
