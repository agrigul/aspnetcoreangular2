﻿import { AuthHttp } from "./auth.http";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Item } from "./item";

@Injectable()
export class ItemService {

    constructor(private http: AuthHttp) { }

    private baseUrl = "api/items/"; // web api URL

    // calls the [GET] /api/items/GetLatest/{n} Web API method to retrieve the latest items.
    getLatest(num?: number): Observable<Item[]> {
        var url: string = this.baseUrl + "GetLatest/";
        if (num != null) { url += num; }
        return this.http.get(url)
            .map((response: Response) => response.json() as Item[])
            .catch(this.handleError);
    }


    // calls the [GET] /api/items/GetMostViewed/{n} Web API method to retrieve the most viewed items.
    getMostViewed(num?: number): Observable<Item[]> {
        var url: string = this.baseUrl + "GetMostViewed/";
        if (num != null) { url += num; }
        return this.http.get(url)
            .map((response: Response) => response.json() as Item[])
            .catch(this.handleError);
    }

    // calls the [GET] /api/items/GetRandom/{n} Web API method to retrieve n random items.
    getRandom(num?: number): Observable<Item[]> {
        var url: string = this.baseUrl + "GetRandom/";
        if (num != null) { url += num; }
        return this.http.get(url)
            .map((response: Response) => response.json() as Item[])
            .catch(this.handleError);
    }

    // calls the [GET] /api/items/{id} Web API method to retrieve the item with the given id.
    get(id: number): Observable<Item> {
        if (id == null) { throw new Error("id is required."); }
        var url: string = this.baseUrl + id;
        return this.http.get(url)
            .map((res: Response) => <Item>res.json())
            .catch(this.handleError);
    }

    
    // calls the [POST] /api/items/ Web API method to add a new item.
    add(item: Item): Observable<Item> {
        var url = this.baseUrl;
        return this.http.post(url, JSON.stringify(item), this.getRequestOptions())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    // calls the [PUT] /api/items/{id} Web API method to update an existingitem.
    update(item: Item): Observable<Item> {
        var url = this.baseUrl + item.Id;
        return this.http.put(url, JSON.stringify(item), this.getRequestOptions())
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    // calls the [DELETE] /api/items/{id} Web API method to delete the item with the given id.
    delete(id: number): Observable<Response> {
        var url = this.baseUrl + id;
        return this.http.delete(url)
            .catch(this.handleError);
    }
    // returns a viable RequestOptions object to handle Json requests
    private getRequestOptions() {
        return new RequestOptions({
            headers: new Headers({
                "Content-Type": "application/json"
            })
        });
    }    private handleError(error: Response): any {
        // output errors to the console.
        console.error(error);
        return Observable.throw(error.json().error || "Server error");
    }
}