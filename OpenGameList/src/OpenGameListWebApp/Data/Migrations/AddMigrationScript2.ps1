﻿## this script recreated DB with Identity User

## to run type from folder with project.json:    & "C:\Sources\AspNetCoreAngualr2\OpenGameList\src\OpenGameListWebApp\Data\Migrations\AddMigrationScript2.ps1"

# 1.1 add generated migration files
dotnet ef migrations add "Identity" -o "Data\Migrations"

# 2. drop update database
dotnet ef database drop
dotnet ef database update

