import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'; // launches JIT compiler for the app
import { enableProdMode } from '@angular/core'; // enables production mode
import { AppModule } from './app/app.module';

// any files imported to main.ts will be added to bundle -> entry point to bundle

if (process.env.ENV === 'production') {
  enableProdMode();
}
platformBrowserDynamic().bootstrapModule(AppModule); //says to bootstrap  which module is a Root Module
