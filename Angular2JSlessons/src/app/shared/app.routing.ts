import { NgModule } from '@angular/core'; // we can have multiple modules besides the root module
import { RouterModule } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { ErrorComponent } from '../error/error.component';

@NgModule({
    // imports ng-module decorators
    imports: [

        // provides rout only for root module, not for children modules
        // follows first match strategy while search for rout
        RouterModule.forRoot([
            { path: '' , component: HomeComponent},
            { path: '**' , component: ErrorComponent } // ** - any rout not configured in an app
        ])    
    ],

    // add this module to RouterModule to make it available to any module which imports RouterModule
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}

