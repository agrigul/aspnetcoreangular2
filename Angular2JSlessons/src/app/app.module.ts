import { NgModule } from '@angular/core'; // helps to configure root module
import { BrowserModule }  from '@angular/platform-browser'; // gives ng-if and ng-for
import { AppComponent } from './start/app.component'; // component which is display when the app starts

import { NavComponent } from './shared/navbar.component';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';
import { AppRoutingModule } from './shared/app.routing';

//TODO: NEXT  - add userService authentiaction in clip  Creating a Service

import { AdminModule } from './admin/admin.module';

// the Root module for multiple modules
// NgModule decorator provides metadata and annotations to our classes
@NgModule({

    // needed for templates declared in this module.
    // this modules will be available to any component which are declared in this module
    imports: [
        BrowserModule, // required for any web app
        AdminModule,
        AppRoutingModule // top -> down policy for routs
    ],

    // for view classes (components, directives or pipes)
    declarations: [
        AppComponent,
        NavComponent,
        HomeComponent,
        ErrorComponent
    ],
    // initiated from main.ts
    bootstrap: [ AppComponent ],

    // allows to use appComponent in other components (optional for app.module).
    // Runs in the sequence: main.ts -> app.module.ts -> app.component.ts
    exports: [AppComponent],

    // allows to import services to our module (optional for app.module)
    providers: [ ]


})
export class AppModule { } // export - makes AppModule available to imported to other components in app
