import { Component } from '@angular/core';



// a component decorator
@Component({
  selector: 'my-app',
  templateUrl: './app.component.html', // path to html
  styleUrls: ['./app.component.css'] // styles applied to component only
})
export class AppComponent { } // is imported by AppModule class
