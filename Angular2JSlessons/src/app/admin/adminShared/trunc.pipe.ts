import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'truncate'}) // pipe allows to edit content inside html

export class TruncatePipe implements PipeTransform {
    transform(value: string, chars: number): string {
        let text = `${value.substring(0, chars)}  ...`;
        return text;
    }
}