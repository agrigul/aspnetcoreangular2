export class Blog{
    constructor(
        public title: string,
        public content: string,
        public imgTitle?: string, // ? - optional property
        public img? : any,
        public id?: string
    ){}
}