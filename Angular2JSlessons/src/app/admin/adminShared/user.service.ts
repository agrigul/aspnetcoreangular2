import { Injectable } from '@angular/core'; // add DI for other components and modules

import {  
    CanActivate, // checks how user is logged on before we navigate to the guarded rout.
    Router,
    ActivatedRouteSnapshot, // will be used, when we process the rout guard.
    RouterStateSnapshot
} from '@angular/router';

import * as firebase from 'firebase';

// decorator
@Injectable()
export class UserService implements CanActivate {
    userLoggedIn: boolean = false;
    loggedInUser: string;
    authUser: any;
    
    constructor( private router: Router ) {
        firebase.initializeApp({
            apiKey: "AIzaSyDnIgMdmIdTac_DdtNak1cM-5Ybs1OGTYw",
            authDomain: "angular2end2end.firebaseapp.com",
            databaseURL: "https://angular2end2end.firebaseio.com",
            projectId: "angular2end2end",
            storageBucket: "angular2end2end.appspot.com",
            messagingSenderId: "487809617800"
        })
     }

     //ActivatedRouteSnapshot - rout which can be activated
    //RouterStateSnapshot - future state of rout if we pass to the rout guard
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean { 
        let url: string = state.url;
        return this.verifyLogin(url);
    }   

    verifyLogin(url: string): boolean {
        if (this.userLoggedIn) { return true; }
                
        this.router.navigate(['/admin/login']);
        return false;
    }

    register(email: string, password: string){
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .catch(function(error) {
                alert(`${error.message} Please Try Again!`);
        });
    }

    verifyUser() {
        this.authUser = firebase.auth().currentUser;
        if (this.authUser) {

            alert(`Welcome ${this.authUser.email}`);
            this.loggedInUser = this.authUser.email;
            this.userLoggedIn = true;
            this.router.navigate(['/admin']);
        }
    }

    login(loginEmail: string, loginPassword: string) {
        firebase.auth().signInWithEmailAndPassword(loginEmail, loginPassword)
            .catch(function(error) {
                alert(`${error.message} Unable to login. Try again!`);
        });
    }

    logout(){
        firebase.auth().signOut().then(function() {
            this.userLoggedIn = false;
            alert(`Logged Out!`);
        }, function(error) {
            alert(`${error.message} Unable to logout. Try again!`);
        });
    }

}


/*
 <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase.js"></script>
 <script>
 // Initialize Firebase
 var config = {
 apiKey: "AIzaSyDnIgMdmIdTac_DdtNak1cM-5Ybs1OGTYw",
 authDomain: "angular2end2end.firebaseapp.com",
 databaseURL: "https://angular2end2end.firebaseio.com",
 projectId: "angular2end2end",
 storageBucket: "angular2end2end.appspot.com",
 messagingSenderId: "487809617800"
 };
 firebase.initializeApp(config);
 </script>
*
* */