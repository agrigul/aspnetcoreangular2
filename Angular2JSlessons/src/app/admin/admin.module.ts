/**
 * Created by Artem on 25.05.2017.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; // directives
import { RouterModule, Routes } from '@angular/router'; // configure routings
import { FormsModule } from '@angular/forms'; // helps to work with forms

import { AdminComponent } from './adminComponent/admin.component';
import { AdminMenuComponent }  from './adminMenu/admin-menu.component';
import { LoginComponent }    from './login/login.component';
import { SignUpComponent }    from './signUp/sign-up.component';

import { UserService } from './adminShared/user.service';
import { BlogAdminService } from './adminShared/blog-admin.service';
import { BlogAdminComponent } from './blogAdmin/blog-admin.component';
import { BlogAddComponent } from './blogAdd/blog-add.component';

import { TruncatePipe } from './adminShared/trunc.pipe';

const AdminRoutes: Routes = [
    {
        path: 'admin',
        component: AdminComponent, // hosts other components
        children: [
            { path: 'blog-admin', component: BlogAdminComponent, canActivate:[UserService] },
            { path: 'login', component: LoginComponent },
            { path: 'signup', component: SignUpComponent },
            { path: '', component: AdminMenuComponent, canActivate: [UserService] } // canActivate allows to protect from unauthorized access
        ]
    },
];
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(AdminRoutes)
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        AdminComponent,
        AdminMenuComponent,
        LoginComponent,
        SignUpComponent,
        BlogAdminComponent,
        BlogAddComponent,
        TruncatePipe
    ],
    providers: [
        UserService,
        BlogAdminService
    ]
})
export class AdminModule {}
