var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');

// web pack handles various files types
// loaders handle compilation

// export allows to load this code from other config files
module.exports = {
  // will be 3 big separate bundle files created
  entry: {
    'polyfills': './src/polyfills.ts',
    'vendor': './src/vendor.ts',
    'app': './src/main.ts' // entry point
  },

  resolve: {
    modulesDirectories: ['node_modules'], // webpack knows how to find the libs that it needs
    extensions: ['', '.js', '.ts', '.scss'] // load only this file types
  },

  // module compiles file in 1 big file (bundle)
  module: {
    loaders: [
      {
        test: /\.ts$/, // typescript loader
        loaders: ['awesome-typescript-loader', 'angular2-template-loader'] // loaders chain from right to left. Webpack
          // will handle for us the typescript compiler
      },
      {
        test: /\.html$/,
        loader: 'html'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file?name=assets/[name].[hash].[ext]'
      },
      {
        test: /\.scss$/, // loads stylesheets
        exclude: helpers.root('src', 'app'), // tells the loader to ignore src/app  file path. Root() specifies to ignore any scss files in app folder
          // we want to process scss files only outside of the project structure (in public folder)
        loader: ExtractTextPlugin.extract('style', 'css!sass') // extract css styles from the bundle, otherwise the be included with the vendor bundle
      },
      {
        test: /\.css$/,
        include: helpers.root('src', 'app'), // use css files inside the app folder. For a component level css
        loader: 'raw' // raw = load content "as is"
      }
    ]
  },

  plugins: [
      // make sure code is not duplicated across bundles
    new webpack.optimize.CommonsChunkPlugin({
      name: ['app', 'vendor', 'polyfills']
    }),

      // helps to dynamically generate stylesheet references
    new HtmlWebpackPlugin({
      template: 'src/index.html' // place references in index.html
    })
    ]
};
