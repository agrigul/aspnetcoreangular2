var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

//webpackMerge  helps to import webpack.common.js into this one
module.exports = webpackMerge(commonConfig, {
  devtool: 'cheap-module-eval-source-map',


    // output generated files settings
  output: {
    path: helpers.root('dist'), // compile files in dist folder, this configuration saves files in memory instead of compiling the files
    publicPath: 'http://localhost:8080/', // path to access the project when it is running on the dev. server
    filename: '[name].js', // name - specify how the bundles will be named
    chunkFilename: '[id].chunk.js'
  },

  plugins: [
      // ???
    new ExtractTextPlugin('[name].css')
  ],
    // settings for a dev server
  devServer: {
    historyApiFallback: true,
    stats: 'minimal'
  }
});
